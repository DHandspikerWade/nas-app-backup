#!/bin/bash
PREFIX="$1"
ansible-galaxy collection install -r requirements.yml && ansible-playbook -i inventory.ini "${PREFIX:=daily}-backup.yml"